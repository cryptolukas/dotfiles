#!/usr/bin/env bash

battery=$(upower -i /org/freedesktop/UPower/devices/battery_hidpp_battery_0 | jc --upower -r | jq '.[] | .detail.percentage' | tr -d '"' | cut -f1 -d" ")
type=$(upower -i /org/freedesktop/UPower/devices/battery_hidpp_battery_0 | jc --upower -r | jq '.[] | .detail.type' | tr -d '"')
model=$(upower -i /org/freedesktop/UPower/devices/battery_hidpp_battery_0 | jc --upower -r | jq '.[] | .model' | tr -d '"')

echo "{\"text\":\"MX Master 2S $battery\"}"
