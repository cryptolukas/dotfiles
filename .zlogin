#!/usr/bin/env zsh

[[ "$TTY" == /dev/tty* ]] || return 0

export $(systemctl --user show-environment)

export LIBSEAT_BACKEND=logind
export WLR_NO_HARDWARE_CURSORS=1
export WLR_DRM_NO_ATOMIC=1 sway
export GPG_TTY="$TTY"
systemctl --user import-environment GPG_TTY SSH_AUTH_SOCK PATH

if [[ -z $DISPLAY && "$TTY" == "/dev/tty1" ]]; then
    systemd-cat -t sway sway
    systemctl --user stop graphical-session.target
    systemctl --user unset-environment DISPLAY WAYLAND_DISPLAY SWAYSOCK
    exit
fi
