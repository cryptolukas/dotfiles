# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH="$HOME/.oh-my-zsh"

ZSH_THEME="agnoster"

plugins=(
  aws
  git
  ssh-agent
  poetry
)

source $ZSH/oh-my-zsh.sh
#export PATH="/home/lukas/.local/bin:$PATH"
#export PATH="/snap/bin:$PATH"
#export PATH="$HOME/.tfenv/bin:$PATH"
export EDITOR=vim

#sonar-scanner
#export SONAR_SCANNER_HOME="/opt/sonar-scanner"
#export PATH="${PATH}:${SONAR_SCANNER_HOME}/bin"

#export PATH=$(python -m site --user-base)/bin:$PATH
#if [ -d "$HOME/.local/bin" ] ; then
#   PATH="$HOME/.local/bin:$PATH"
#fi

export GPG_TTY=$(tty)
setopt inc_append_history     # add commands to HISTFILE in order of execution
setopt share_history          # share command history data

#Wayland
export QT_QPA_PLATFORM=wayland
export MOZ_ENABLE_WAYLAND=1
export MOZ_DBUS_REMOTE=1
export _JAVA_AWT_WM_NONREPARENTING=1
export SDL_VIDEODRIVER=wayland
export XDG_CURRENT_DESKTOP=sway
export XDG_SESSION_TYPE=wayland
export GDK_BACKEND=wayland
export CLUTTER_BACKEND=wayland

alias df='df -hT | grep -vE "squashfs|tmpfs|efi"'
alias ll='ls -lah'
alias tf='terraform'

# pipx completions
#autoload -U +X bashcompinit && bashcompinit

#autoload -Uz compinit
#zstyle ':completion:*' menu select
#fpath+=~/.zfunc

# Created by `pipx` on 2023-05-26 12:16:21
export PATH="$PATH:/home/lukas/.local/bin"
